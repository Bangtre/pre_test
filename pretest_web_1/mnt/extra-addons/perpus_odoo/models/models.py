from odoo.exceptions import ValidationError
from odoo import models, fields, api
from datetime import timedelta, datetime, date

class transaksi(models.Model):
    _name = 'transaksi.perpus'
    _description = 'Transaksi Rental'

               
    # @api.constrains('durasi', 'biaya')
    # def compute_rent_price(self):
    #     for date in self:
    #         if date.tanggal_pinjam > date.tanggal_kembali:
    #             raise ValidationError('Waktu kembali tidak bisa melebihi waktu pinjam')
    #         else:
    #             date.durasi = (date.tanggal_kembali - date.tanggal_pinjam).days
    
    @api.model
    def create(self, values):
        values['ref'] = self.env['ir.sequence'].next_by_code('transaksi.perpus')
        return super(transaksi, self).create(values)

    rental_date = fields.Date(string='Tanggal Rental')
    peminjam = fields.Many2one('member.perpus', string='Nama Peminjam', required=True, ondelete='cascade')
    tanggal_pinjam = fields.Date(string='Tanggal Pinjam')
    tanggal_kembali = fields.Date(string='Tanggal Kembali')
    durasi = fields.Float(string='Durasi Peminjaman', readonly=True)
    biaya = fields.Char(string='Total biaya Sewa', compute='compute_')
    list_buku = fields.One2many('perpus.pretes', 'buku_id', string='Buku dipinjam')
    ref = fields.Char(string='Referensi', readonly=True, default='/')

class perpus_odoo(models.Model):
    _name = 'perpus.pretes'
    _description = 'Perpustakaan'
    _sql_constraints = [
    ('kode_isbn_unik', 'unique(kode)', 'Kode ISBN Tidak Boleh Sama')
    
    ]

    buku_id = fields.Many2one('transaksi.perpus', string='Peminjam', ondelete='cascade')
    judul = fields.Char(string="Judul Buku")
    kategori = fields.Selection([('gen', 'Umum'), ('tech', 'IT'), ('med', 'Kesehatan'), ('pol', 'Politik')])
    tanggal = fields.Date(string='Tanggal Terbit')
    penulis = fields.Many2many('res.partner', string='Penulis')
    kode = fields.Text(string="Kode ISBN", unique=True)


class member(models.Model):
    _name = 'member.perpus'
    _description = 'Member Perpustakaan'
    _sql_constraints = [
    ('nomor_identitas_unik', 'unique(user_id)', 'Nomor Identitas Tidak Boleh Sama')
    
    ]

    name = fields.Char(string="Nama")
    user_id = fields.Text(string = "Nomor Kartu Identitas", unique=True)
    jeniskartu = fields.Selection([('ktp', 'KTP'), ('sim', 'SIM'), ('pass', 'Passport')], string="Pilih Kartu Identisa", required=True)
    peminjam_id = fields.One2many('transaksi.perpus', 'peminjam', string='Peminjam')
    state = fields.Selection([('draft', 'Draft'), ('approve', 'Approved')], string='State', default='draft')